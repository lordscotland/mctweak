#import <EventKit/EventKit.h>

static Ivar v_secondaryLabel;
%hook EKUIOccurrenceTableViewCell
-(void)_updateSecondaryTextLabel {
  %orig;
  NSString* string=objc_getAssociatedObject(self,&v_secondaryLabel);
  if(string){
    UILabel* label=object_getIvar(self,v_secondaryLabel);
    NSAttributedString* text=label.attributedText;
    if(text){
      NSMutableAttributedString* newText=text.mutableCopy;
      [newText replaceCharactersInRange:NSMakeRange(0,0)
       withString:[string stringByAppendingFormat:@" \u2022 "]];
      [label.attributedText=newText release];
    }
    else {label.text=string;}
  }
}
-(void)updateWithEvent:(EKEvent*)event calendar:(NSCalendar*)calendar placedUnderDayWithStartDate:(NSDate*)date opaque:(BOOL)opaque drawsDimmedForPast:(BOOL)drawsDimmedForPast includingTravelTime:(BOOL)includingTravelTime includingCountdown:(BOOL)includingCountdown {
  NSDate* fromDate=[calendar startOfDayForDate:event.startDate];
  NSInteger ndays=[calendar components:NSCalendarUnitDay
   fromDate:fromDate toDate:event.endDate options:0].day;
  objc_setAssociatedObject(self,&v_secondaryLabel,(ndays>0)?[NSString
   stringWithFormat:@"Day %zd/%zd",[calendar components:NSCalendarUnitDay
   fromDate:fromDate toDate:date options:0].day+1,ndays+1]:nil,
   OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  %orig;
}
%end

%ctor {
  v_secondaryLabel=class_getInstanceVariable(%c(EKUIOccurrenceTableViewCell),"_secondaryLabel");
  %init;
}
