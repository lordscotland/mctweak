#import <EventKit/EventKit.h>

#define MAXROWS 4

static const NSString* _ItemParams=@"ItemParams";
static const CGFloat _EventBarCornerRadius=4;
static const CGFloat _EventBarMargin=2;
static const CGFloat _EventBarHeight=12;

@interface EKCalendarDate
-(NSDate*)date;
@end

@interface CalendarOccurrencesCollection
@property(readonly) NSArray* occurrences;
@end

@interface CalendarModel
-(CalendarOccurrencesCollection*)occurrencesForStartDate:(NSDate*)startDate endDate:(NSDate*)endDate preSorted:(BOOL)preSorted waitForLoad:(BOOL)waitForLoad;
@end

@interface CompactMonthWeekDayNumber
@property(assign) EKCalendarDate* calendarDate;
@property(assign) CALayer* eventMarker;
@property(assign) CGRect frame;
@property(readonly) CALayer* layer;
@end

%hook Application
@interface Application : UIApplication
@property(assign) CalendarModel* model;
@end
-(void)setLastActiveTime:(NSDate*)date {
  %orig(nil);
}
%end

%hook CompactMonthWeekTodayCircle
-(void)setBackgroundColor:(UIColor*)color {
  %orig([UIColor clearColor]);
}
%end

%hook TappableDayNumber
-(void)setBackgroundColor:(UIColor*)color {
  %orig([UIColor clearColor]);
}
%end

%hook CompactWidthMonthViewController
-(CGFloat)_showDateVerticalOffsetForMode:(BOOL)compressedVerticalMode {
  return %orig+(compressedVerticalMode?0:16);
}
%end

%hook CompactMonthWeekView
static ptrdiff_t o_cellToStartDrawingIn;
static Ivar v_calendar,v_days,v_grayLine,v_monthNameLabel;
@interface CompactMonthWeekView : UIView
@property(assign) BOOL compressedVerticalMode;
@property(assign) EKCalendarDate* calendarDate;
@property(readonly) EKCalendarDate* endCalendarDate;
@end
+(CGFloat)_spaceBelowGrayLineHeight:(BOOL)compressedVerticalMode {
  return %orig+(compressedVerticalMode?0:12);
}
-(void)layoutSubviews {
  %orig;
  [CATransaction begin];
  [CATransaction setDisableActions:YES];
  NSArray* eventBars=objc_getAssociatedObject(self,&v_calendar);
  NSArray* days=object_getIvar(self,v_days);
  BOOL hideMarkers=NO;
  if(self.compressedVerticalMode){
    for (CALayer* eventBar in eventBars){eventBar.hidden=YES;}
  }
  else {
    UIView* grayLine=object_getIvar(self,v_grayLine);
    grayLine.frame=CGRectOffset(grayLine.frame,0,8);
    UILabel* monthNameLabel=object_getIvar(self,v_monthNameLabel);
    monthNameLabel.frame=CGRectOffset(monthNameLabel.frame,0,12);
    if(eventBars){
      hideMarkers=YES;
      const unsigned int ifirst=*(NSInteger*)((char*)self+o_cellToStartDrawingIn);
      const CGFloat y0=CGRectGetMaxY(((CompactMonthWeekDayNumber*)
       [days objectAtIndex:ifirst]).layer.frame)+_EventBarMargin;
      const CGFloat yspace=_EventBarHeight+_EventBarMargin;
      for (CALayer* eventBar in eventBars){
        unsigned int params[3];
        [[eventBar.style objectForKey:_ItemParams] getValue:params];
        const unsigned int i=ifirst+params[0],iend=ifirst+params[1];
        CGRect frame=((CompactMonthWeekDayNumber*)[days objectAtIndex:i]).frame;
        CGFloat x=CGRectGetMinX(frame),width=CGRectGetMaxX(i==iend?frame:
         ((CompactMonthWeekDayNumber*)[days objectAtIndex:iend]).frame)-x-1;
        eventBar.frame=CGRectMake(x,y0+params[2]*yspace,width,_EventBarHeight);
        ((CATextLayer*)[eventBar.sublayers objectAtIndex:0])
         .frame=CGRectMake(_EventBarMargin,0,width-_EventBarMargin,_EventBarHeight);
        eventBar.hidden=NO;
      }
    }
  }
  for (CompactMonthWeekDayNumber* day in days){day.eventMarker.hidden=hideMarkers;}
  [CATransaction commit];
}
-(void)setEventCounts:(NSArray*)counts animated:(BOOL)animated {
  %orig;
  NSArray* eventBars=objc_getAssociatedObject(self,&v_calendar);
  for (CALayer* eventBar in eventBars){[eventBar removeFromSuperlayer];}
  eventBars=nil;
  for (NSNumber* number in counts){
    if(!number.intValue){continue;}
    NSDate* fromDate=self.calendarDate.date;
    NSArray* events=[((Application*)[UIApplication sharedApplication]).model
     occurrencesForStartDate:fromDate endDate:self.endCalendarDate.date
     preSorted:YES waitForLoad:NO].occurrences;
    if(!events.count){break;}
    const unsigned int ndays=counts.count;
    struct eventlist {
      unsigned int nspan,ntotal;
      struct eventitem {
        EKEvent* event;
        unsigned int iend;
        BOOL xstart,xend;
      } items[MAXROWS];
      BOOL dodge[MAXROWS];
    }* daylists=malloc(ndays*sizeof(*daylists));
    unsigned int nobjs=0,i;
    for (i=0;i<ndays;i++){
      struct eventlist* list=&daylists[i];
      list->nspan=list->ntotal=0;
      memset(list->dodge,0,MAXROWS*sizeof(BOOL));
    }
    NSCalendar* calendar=object_getIvar(self,v_calendar);
    for (EKEvent* event in events){
      NSDate* startDate=event.startDate;
      BOOL xstart=[fromDate compare:startDate]==NSOrderedDescending;
      int istart=xstart?0:[calendar components:NSCalendarUnitDay
       fromDate:fromDate toDate:startDate options:0].day;
      int iend=[calendar components:NSCalendarUnitDay
       fromDate:fromDate toDate:event.endDate options:0].day;
      BOOL xend=(iend>ndays-1);
      if(xend){iend=ndays-1;}
      struct eventlist* list=&daylists[istart];
      unsigned int n=list->ntotal++;
      if(iend>istart){
        const unsigned int nx=list->nspan;
        if(nx>=MAXROWS){continue;}
        const int nshift=(n>MAXROWS-1?MAXROWS-1:n)-nx;
        if(nshift>0){
          struct eventitem* item=&list->items[nx];
          memmove(item+1,item,nshift*sizeof(*item));
        }
        list->nspan=(n=nx)+1;
      }
      else if(n>=MAXROWS){continue;}
      list->items[n]=(struct eventitem){event,iend,xstart,xend};
      nobjs++;
    }
    const CGFloat scale=[UIScreen mainScreen].scale;
    const CGFloat fontSize=10;
    NSString* fontNames[2]={nil};
    CALayer* container=self.layer;
    CGContextRef context=NULL;
    CALayer** objbuf=malloc(nobjs*sizeof(*objbuf));
    nobjs=0;
    for (i=0;i<ndays;i++){
      const struct eventlist* list=&daylists[i];
      const unsigned int ntotal=list->ntotal;
      unsigned int n,n2row[MAXROWS],row=0;
      for (n=0;n<ntotal;n++){
        while(row<MAXROWS && list->dodge[row]){row++;}
        if(row==MAXROWS){break;}
        n2row[n]=row++;
      }
      NSString* fontName=(n<ntotal)?
       fontNames[1]?:(fontNames[1]=[UIFont italicSystemFontOfSize:fontSize].fontName):
       fontNames[0]?:(fontNames[0]=[UIFont boldSystemFontOfSize:fontSize].fontName);
      const unsigned int nshow=n;
      for (n=0;n<nshow;n++){
        const struct eventitem* item=&list->items[n];
        const unsigned int iend=item->iend,row=n2row[n];
        unsigned int inext=i;
        while(++inext<=iend){daylists[inext].dodge[row]=YES;}
        EKEvent* event=item->event;
        CGColorRef bgcolor=event.calendar.CGColor;
        CALayer* eventBar=[CALayer layer];
        if(item->xstart==item->xend){
          eventBar.backgroundColor=bgcolor;
          if(!item->xstart){eventBar.cornerRadius=_EventBarCornerRadius;}
        }
        else {
          const CGFloat radius=_EventBarCornerRadius*scale;
          const CGFloat csize=radius*2;
          if(context){CGContextClearRect(context,CGRectMake(0,0,csize,csize));}
          else {
            CGColorSpaceRef cspace=CGColorSpaceCreateDeviceRGB();
            context=CGBitmapContextCreate(NULL,csize,csize,8,0,cspace,
             (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
            CGColorSpaceRelease(cspace);
          }
          const CGFloat x=item->xstart?0:csize;
          CGContextMoveToPoint(context,x,0);
          CGContextAddLineToPoint(context,x,csize);
          CGContextAddArc(context,radius,radius,radius,M_PI/2,3*M_PI/2,item->xstart);
          CGContextSetFillColorWithColor(context,bgcolor);
          CGContextFillPath(context);
          CGImageRef img=CGBitmapContextCreateImage(context);
          eventBar.contents=(id)img;
          CGImageRelease(img);
          eventBar.contentsCenter=CGRectMake(0.5,0.5,0,0);
          eventBar.contentsScale=scale;
        }
        eventBar.hidden=YES;
        const unsigned int params[]={i,iend,row};
        eventBar.style=[NSDictionary dictionaryWithObject:[NSValue value:&params
         withObjCType:@encode(__typeof__(params))] forKey:_ItemParams];
        CATextLayer* titleLayer=[[CATextLayer alloc] init];
        titleLayer.string=event.title;
        titleLayer.font=fontName;
        titleLayer.fontSize=fontSize;
        titleLayer.contentsScale=scale;
        [eventBar addSublayer:titleLayer];
        [titleLayer release];
        [container addSublayer:eventBar];
        objbuf[nobjs++]=eventBar;
      }
    }
    free(daylists);
    CGContextRelease(context);
    if(nobjs){eventBars=[NSArray arrayWithObjects:objbuf count:nobjs];}
    free(objbuf);
    break;
  }
  objc_setAssociatedObject(self,&v_calendar,eventBars,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
%end

%ctor {
  Class cls=%c(CompactMonthWeekView);
  o_cellToStartDrawingIn=ivar_getOffset(class_getInstanceVariable(cls,"_cellToStartDrawingIn"));
  v_calendar=class_getInstanceVariable(cls,"_calendar");
  v_days=class_getInstanceVariable(cls,"_days");
  v_grayLine=class_getInstanceVariable(cls,"_grayLine");
  v_monthNameLabel=class_getInstanceVariable(cls,"_monthNameLabel");
  %init;
}
