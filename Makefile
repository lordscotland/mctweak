ARCHS = arm64

include theos/makefiles/common.mk

TWEAK_NAME = Calendar
Calendar_FILES = Tweak.x
Calendar_FRAMEWORKS = CoreGraphics QuartzCore UIKit

TWEAK_NAME += Calendar.TW
Calendar.TW_FILES = widget.x
Calendar.TW_FRAMEWORKS = UIKit

include $(THEOS_MAKE_PATH)/tweak.mk
